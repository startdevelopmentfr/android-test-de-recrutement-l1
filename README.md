# Android  Test de recrutement L1
 
 ** Welcome to our Mobile  simple  Technical Test **

** please read the instructions carefully before starting **
Please feel free to spend as much time as you deem necessary and let us know how long you spent in your ** Email ** .

Your goal is to create an APP for the star wars fans : where users can find Star Wars planets list at first  .then by selecting a specific planet they will be able to read more details about it , that it :) [Find the design link](https://www.figma.com/file/LiVktubt9spaTaccYP6fJC/Sample-File?node-id=0%3A258) ,
Don't worry about assets you can use you're own  Make sure you use the Star wars API for the work.[star wars api](https://swapi.co/)
.
There is documentation down Below which describes the endpoint you will need for this App but make sure to check the official documentation [here](https://swapi.co/documentation#planets). 
 * you have the option to choose only one language to create your solution 
 ⋅⋅⋅ Please use java or kotlin if you'd like to work with Android  .
 - Send us your code (we recommend using git) 
 #Technical questions
** Please answer the following questions in a markdown file called Answers to technical questions.md. **

* How long did you spend on the coding test? 
* What would you add to your solution if you had more time? 
* How would you improve the  app you just made ?

#Api Documentation

###List of endpoints to implement#

- get Planets
``` 
http https://swapi.co/api/planets
```

- Get planet by id   
```
GET /Planet by id 
http https://swapi.co/api/planets/1/
```
 


